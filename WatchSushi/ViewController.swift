//
//  ViewController.swift
//  WatchSushi
//
//  Created by Harman Kaur on 2019-11-03.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import UIKit
import WatchConnectivity
import SpriteKit
import GameplayKit
import Firebase


class ViewController: UIViewController {
    
    var gameScene : GameScene!
    
    @IBOutlet weak var btnEnter: UIButton!
    @IBOutlet weak var playerNameTxtField: UITextField!
    
    @IBAction func btnEnterPressed(_ sender: UIButton) {
        guard let name =  playerNameTxtField.text, !name.isEmpty else {
            showMessage(message: "Please enter name")
            return
        }
        getHighScore(name: name)
    }
    
    func showMessage(message: String) {
        
        let alertController = UIAlertController.init(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        
        alertController.addAction(action)
        
        self.present(alertController, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = GameScene(size:self.view.bounds.size)
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        scene.scaleMode = .aspectFill

        // property to show hitboxes
        skView.showsPhysics = true

        skView.presentScene(scene)
        
    
    }
    
    
    func initializeGameScene(highScore: Int) {

        playerNameTxtField.isHidden = true
        btnEnter.isHidden = true
        gameScene = GameScene(size: self.view.bounds.size)
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        skView.ignoresSiblingOrder = true
        // property to show hitboxes
        skView.showsPhysics = true
        gameScene.scaleMode = .aspectFill
        gameScene.viewController = self
        self.gameScene.highScore = highScore
        self.gameScene.highScoreLabel.text = "High Score: \(highScore)"
        skView.presentScene(gameScene)
    }


}

extension ViewController{
    func saveHighScore(name: String) {
        print("x")
        let ref: DatabaseReference! = Database.database().reference()
        let highScore = gameScene.score
        ref.child(name).setValue(highScore)
    }
    
    func getHighScore(name: String) {
        if !name.isEmpty {
            let ref: DatabaseReference! = Database.database().reference()
            ref.child(name).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                let value = snapshot.value as? Int
                self.initializeGameScene(highScore: value ?? 0)
            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }
}

