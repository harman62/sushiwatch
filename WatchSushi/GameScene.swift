//
//  GameScene.swift
//  WatchSushi
//
//  Created by Harman Kaur on 2019-11-03.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import SpriteKit
import GameplayKit
import WatchConnectivity
import Firebase

class GameScene: SKScene, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func sendMessageToWatch(messageDictionary: [String: Any]) {
        
        if WCSession.default.isReachable {
            WCSession.default.sendMessage(messageDictionary, replyHandler: nil)
        }
        else {
            print("Watch is not reachable")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        
        DispatchQueue.main.async {
            if (message.keys.contains("catMoving")){
                print("Phone: Command received to move cat from watch")
                //set move direction to left/right based on message recieved and call function
                self.catMoveDirection = message["catMoving"] as! String
                self.moveCat()
            }
            if (message.keys.contains("MoreTime")) {
                if (self.timeRemaining <= 15) {
                    // add 10 seconds to Time remaining
                    self.timeRemaining = self.timeRemaining + 10
                    //update timeBar width and position
//                    self.timeBar.size.width = self.timeBar.size.width + 100
//                    self.timeBar.position.x = self.timeBar.position.x + 50
                }
                else {
                    // increase seconds remaining to 25
                    self.timeRemaining = 25
                    //update timeBar width and position
//                    self.timeBar.size.width = 250
//                    self.timeBar.position.x = self.size.width/2
                }
            }
            
            if(message.keys.contains("pauseGame"))
            {
                self.timeRemainingLabel.text = "GAME PAUSED"
                self.scene!.view?.isPaused = true
                    self.gamePaused = true
                print("pause")
                    
                }
            else
            {
                    self.gamePaused = false
                }
            
            if(message.keys.contains("NameOfHighScorer")){
                let highScoreName = message["NameOfHighScorer"] as? String
                self.saveHighScore(name: highScoreName!)
                print(highScoreName)
            }
//            if let highScoreName = message["NameOfHighScorer"] as? String {
//                self.viewController.saveHighScore(name: highScoreName)
//                print(highScoreName)
//            }
            
            
        }
    
    }
    var viewController : ViewController!
    let timeRemainingLabel = SKLabelNode(text: "25")
    var timeRemaining = 25
    var highScore = 0
    let cat = SKSpriteNode(imageNamed: "character1")
    let sushiBase = SKSpriteNode(imageNamed:"roll")
    var updateTime = 1
    // Make a tower
    var sushiTower :[SushiPiece] = []
    let SUSHI_PIECE_GAP:CGFloat = 80
    var catPosition = "left"
    
    var powerUpRemaining = 2
    var powerUp1 = Int.random(in: 1 ..< 15)
    var powerUp2 = Int.random(in: 16 ..< 25)
    
    var powerUpTime = 0
    var activatePowerUp = false
    var gameOver = false
    var gamePaused = false
    
    
    // Show life and score labels
    let lifeLabel = SKLabelNode(text:"Lives: ")
    let scoreLabel = SKLabelNode(text:"Score: ")
    let highScoreLabel = SKLabelNode(text:"High Score: ")

    
    var lives = 5
    var score = 0
    
    var catMoveDirection = "left"
   
    
    
    func spawnSushi() {
        
        // -----------------------
        // MARK: PART 1: ADD SUSHI TO GAME
        // -----------------------
        
        // 1. Make a sushi
        let sushi = SushiPiece(imageNamed:"roll")
        
        // 2. Position sushi 10px above the previous one
        if (self.sushiTower.count == 0) {
            // Sushi tower is empty, so position the piece above the base piece
            sushi.position.y = sushiBase.position.y
                + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        else {
            let previousSushi = sushiTower[self.sushiTower.count - 1]
            sushi.position.y = previousSushi.position.y + SUSHI_PIECE_GAP
            sushi.position.x = self.size.width*0.5
        }
        
        // 3. Add sushi to screen
        addChild(sushi)
        
        // 4. Add sushi to array
        self.sushiTower.append(sushi)
    }
    
    override func didMove(to view: SKView) {
        
        
        DispatchQueue.main.async {
            // 1. Check if phone supports WCSessions
            print("Phone view loaded")
            if WCSession.isSupported() {
                print("Phone supports WCSession")
                WCSession.default.delegate = self
                WCSession.default.activate()
                print("Session Activated")
            }
            else {
                print("Phone does not support WCSession")
            }
        }
        
        
        
        // add background
        let background = SKSpriteNode(imageNamed: "background")
        background.size = self.size
        background.position = CGPoint(x: self.size.width / 2, y: self.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        // add cat
        cat.position = CGPoint(x:self.size.width*0.25, y:100)
        addChild(cat)
        
        // add base sushi pieces
        sushiBase.position = CGPoint(x:self.size.width*0.5, y: 100)
        addChild(sushiBase)
        
        // build the tower
        self.buildTower()
        
        self.timeRemainingLabel.position = CGPoint(x: 150, y: self.size.height - 70)
        self.timeRemainingLabel.fontName = "Avenir"
        self.timeRemainingLabel.fontSize = 40
        self.timeRemainingLabel.zPosition = 30
        self.timeRemainingLabel.fontColor = UIColor.red
        addChild(timeRemainingLabel)
        

        
        // Game labels
        self.scoreLabel.position.x = 100
        self.scoreLabel.position.y = size.height - 100
        self.scoreLabel.fontName = "Avenir"
        self.scoreLabel.zPosition = 30
        self.scoreLabel.fontSize = 40
        self.scoreLabel.color = UIColor.green
        addChild(scoreLabel)
        
        // Life label
        self.lifeLabel.position.x = 100
        self.lifeLabel.position.y = size.height - 150
        self.lifeLabel.fontName = "Avenir"
        self.lifeLabel.fontSize = 40
        self.lifeLabel.zPosition = 30
        self.lifeLabel.color = UIColor.green
        addChild(lifeLabel)
        
        // High Score label
        self.highScoreLabel.position.x = 10
        self.highScoreLabel.position.y = size.height - 50
        self.highScoreLabel.fontName = "Avenir"
        self.highScoreLabel.fontSize = 20
        self.highScoreLabel.horizontalAlignmentMode = .left
        self.highScoreLabel.zPosition = 30
        self.highScoreLabel.color = UIColor.red
        addChild(highScoreLabel)
        
    }
    
    func buildTower() {
        for _ in 0...10 {
            self.spawnSushi()
        }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        updateTime = updateTime + 1
        if( updateTime % 60 == 0 && self.timeRemaining > 0){
            timeRemaining = timeRemaining - 1
            self.updatePowerUp()
            if self.timeRemaining == 5 || self.timeRemaining == 10 || self.timeRemaining == 15 {
                sendMessageToWatch(messageDictionary: ["Timer": "\(self.timeRemaining) seconds left"])
            }
            else {
                sendMessageToWatch(messageDictionary: ["Timer": ""])
            }
            timeRemainingLabel.text = "Time Left: \(timeRemaining)"
        
        }
        if(self.timeRemaining == 0)
        {
            //scene!.view?.isPaused = true
            self.timeRemainingLabel.fontColor = UIColor.red
            self.timeRemainingLabel.text = "GAME OVER"
            gameOver = true
            self.sendGameOverToWatch()
        }
        
    }
    func sendGameOverToWatch() {
        
        var dictionary = [String: Any]()
        dictionary["Over"] = "Game Over"
        
        if isHighScore() {
            dictionary["HighScore"] = true
        }
        self.sendMessageToWatch(messageDictionary: dictionary)
    }
    
    func isHighScore() -> Bool {
        if self.score > self.highScore {
            return true
        }
        return false
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        
        // This is the shortcut way of saying:
        //      let mousePosition = touches.first?.location
        //      if (mousePosition == nil) { return }
        guard let mousePosition = touches.first?.location(in: self) else {
            return
        }
        
        print(mousePosition)
        

        let middleOfScreen  = self.size.width / 2
        if (mousePosition.x < middleOfScreen) {
            print("TAP LEFT")
            self.catMoveDirection = "left"
            self.moveCat()
            
            
        }
        else {
            print("TAP RIGHT")
            self.catMoveDirection = "right"
            self.moveCat()
            
        }
        
        
        
    }
    
    
    public func moveCat(){
        //move the cat left/right
        if(!gameOver && !gamePaused){
            let pieceToRemove = self.sushiTower.first
            if (pieceToRemove != nil) {
                // SUSHI: hide it from the screen & remove from game logic
                pieceToRemove!.removeFromParent()
                self.sushiTower.remove(at: 0)
                
                // SUSHI: loop through the remaining pieces and redraw the Tower
                for piece in sushiTower {
                    piece.position.y = piece.position.y - SUSHI_PIECE_GAP
                }
                
                // To make the tower inifnite, then ADD a new piece
                self.spawnSushi()
            }
            if self.catMoveDirection == "left" {
                cat.position = CGPoint(x:self.size.width*0.25, y:100)
                
                // change the cat's direction
                let facingRight = SKAction.scaleX(to: 1, duration: 0)
                self.cat.run(facingRight)
                
                // save cat's position
                self.catPosition = "left"
            }
            if self.catMoveDirection == "right"{
                cat.position = CGPoint(x:self.size.width*0.85, y:100)
                
                // change the cat's direction
                let facingLeft = SKAction.scaleX(to: -1, duration: 0)
                self.cat.run(facingLeft)
                
                // save cat's position
                self.catPosition = "right"
                
            }
            // ------------------------------------
            // MARK: ANIMATION OF PUNCHING CAT
            // -------------------------------------
            
            // show animation of cat punching tower
            let image1 = SKTexture(imageNamed: "character1")
            let image2 = SKTexture(imageNamed: "character2")
            let image3 = SKTexture(imageNamed: "character3")
            
            let punchTextures = [image1, image2, image3, image1]
            
            let punchAnimation = SKAction.animate(
                with: punchTextures,
                timePerFrame: 0.1)
            
            self.cat.run(punchAnimation)
            
            
            // ------------------------------------
            // MARK: WIN AND LOSE CONDITIONS
            // -------------------------------------
            
            if (self.sushiTower.count > 0) {
                // 1. if CAT and STICK are on same side - OKAY, keep going
                // 2. if CAT and STICK are on opposite sides -- YOU LOSE
                let firstSushi:SushiPiece = self.sushiTower[0]
                let chopstickPosition = firstSushi.stickPosition
                
                if (catPosition == chopstickPosition) {
                    // cat = left && chopstick == left
                    // cat == right && chopstick == right
                    print("Cat Position = \(catPosition)")
                    print("Stick Position = \(chopstickPosition)")
                    print("Conclusion = LOSE")
                    print("------")
                    
                    self.lives = self.lives - 1
                    if (self.lives < 1){
                        //scene!.view?.isPaused = true
                        self.timeRemainingLabel.fontColor = UIColor.red
                        self.timeRemainingLabel.text = "GAME OVER"
                        gameOver = true
                        self.sendGameOverToWatch()
                        
                    }
                    self.lifeLabel.text = "Lives: \(self.lives)"
                }
                else if (catPosition != chopstickPosition) {
                    // cat == left && chopstick = right
                    // cat == right && chopstick = left
                    print("Cat Position = \(catPosition)")
                    print("Stick Position = \(chopstickPosition)")
                    print("Conclusion = WIN")
                    print("------")
                    
                    self.score = self.score + 10
                    self.scoreLabel.text = "Score: \(self.score)"
                }
            }
                
            else {
                print("Sushi tower is empty!")
            }
        }
        
    }
    
    func updatePowerUp() {
        
        if self.timeRemaining == self.powerUp1 || self.timeRemaining == self.powerUp2 {
            self.sendMessageToWatch(messageDictionary: ["PowerUp" : true])
            self.activatePowerUp = true
        }
        
        if self.activatePowerUp {
            self.powerUpTime = self.powerUpTime + 1
        }
        
        if self.powerUpTime >= 2 {
            self.activatePowerUp = false
            self.powerUpTime = 0
            self.sendMessageToWatch(messageDictionary: ["PowerUp" : false])
        }
    }
    
    func saveHighScore(name: String) {
        print("x")
        let ref: DatabaseReference! = Database.database().reference()
        let highScore = score
        ref.child(name).setValue(highScore)
    }
    
    
    
}

