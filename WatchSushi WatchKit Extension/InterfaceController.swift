//
//  InterfaceController.swift
//  WatchSushi WatchKit Extension
//
//  Created by Harman Kaur on 2019-11-03.
//  Copyright © 2019 Harman Kaur. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
   
    @IBOutlet weak var btnTimer: WKInterfaceButton!
    
    @IBOutlet weak var btnLeft: WKInterfaceButton!
    @IBOutlet weak var btnRight: WKInterfaceButton!
    
    @IBOutlet weak var gestureLabel: WKInterfaceLabel!
    @IBOutlet weak var timerLabel: WKInterfaceLabel!
    
    @IBAction func longPressGesture(_ sender: Any) {
        if let _ = sender as? WKLongPressGestureRecognizer {
            if isPause {
                gestureLabel.setText("Tap to Pause")
                isPause = false
            } else {
                gestureLabel.setText("Tap to Resume")
                isPause = true
            }
            
            if WCSession.default.isReachable {
                WCSession.default.sendMessage(
                    ["pauseGame" : pause],
                    replyHandler: {
                        (_ replyMessage: [String: Any]) in
                }, errorHandler: { (error) in
                })
            }
            else {
                print("Phone is not reachable")
            }
        }
    }
    var isPause: Bool = false
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    @IBAction func btnTimerPressed() {
        
        if (WCSession.default.isReachable) {
            print("Phone is reachable")
            let message = ["MoreTime": true]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("Watch: Powerup accepted")
        }
        else {
            print("WATCH: Cannot reach phone")
        }
    }
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        if WCSession.isSupported() {
            print("Watch supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("WC session activated")
        }
        else {
            print("Watch does not support WCSession")
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        if ((message["PowerUp"] as? Bool) != nil ){
            self.btnTimer.setHidden(false)
        } else {
            self.btnTimer.setHidden(true)
        }
        
        if let timeLeft = message["Timer"] as? String {
            self.timerLabel.setText("Time Left:  \(timeLeft)")
        } else {
            self.timerLabel.setText("")
        }
        
        // For Game Over
        if let gameOverMessage = message["Over"] as? String {
            self.timerLabel.setText(gameOverMessage)
            self.gameOver()
            
            if let isHighScore = message["HighScore"] as? Bool, isHighScore {
                self.presentTextInputController(withSuggestions: ["abc","def","efg"], allowedInputMode: WKTextInputMode.plain,
                                                               completion:{(results) -> Void in
                                                                let aResult = results?[0] as? String
                                                                print(aResult ?? "")
                })
                    
                }
            }
        
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
    
    @IBAction func btnRightPressed() {
        if (WCSession.default.isReachable) {
            print("Phone is reachable")
            let message = ["catMoving": "right"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("Watch: move cat to right in the phone")
        }
        else {
            print("WATCH: Cannot reach phone")
        }
    }
    @IBAction func btnLeftPressed() {
        if (WCSession.default.isReachable) {
            print("Phone is reachable")
            let message = ["catMoving": "left"]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("Watch: move cat to right in the phone")
        }
        else {
            print("WATCH: Cannot reach phone")
        }
    }
    
    func gameOver() {
        self.btnLeft.setEnabled(false)
        self.btnRight.setEnabled(false)
    }
    
    func inputName() {
        // Enter name and send the score to cloud firebase
        let cannedResponses = ["abc", "def", "ghi", "jkl"]
        presentTextInputController(withSuggestions: cannedResponses, allowedInputMode: .plain) { (results) in
            
            
            if (results != nil && results!.count > 0) {
                let userResponse = results?.first as? String
                self.sendNameOfHighScorer(name: userResponse!)
                print(userResponse ?? "")
            }
        
        }
    }
    
    func sendNameOfHighScorer(name: String) {
        
        if (WCSession.default.isReachable) {
            print("Phone is reachable")
            print(name)
            let message = ["NameOfHighScorer": name]
            WCSession.default.sendMessage(message, replyHandler: nil)
            print("Watch: send name of player to phone")
        }
        else {
            print("Phone is not reachable")
        }
    }
}
    

